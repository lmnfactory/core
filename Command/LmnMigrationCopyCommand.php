<?php

namespace Lmn\Core\Command;

use Illuminate\Console\Command;

class LmnMigrationCopyCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lmn:migration-copy {--clear} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy migraion files from modules to app database migrations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appMigrationPath = "database/migrations/";
        $path = config("module.path", false);
        $module = config("module.module", false);

        if ($module === false){
            $this->error("module.php config does not contain module property.");
        }
        if ($path === false){
            $this->error("module.php config does not contain path property.");
        }

        $pathOption = $this->option("path");
        if ($pathOption == null){
            $pathOption = "/Database/Migration";
        }

        //Clear folder
        if ($this->option("clear")){
            $files = scandir($appMigrationPath);
            if ($files !== false){
                foreach($files as $file){
                  if(is_file($appMigrationPath.$file))
                    unlink($appMigrationPath.$file);
                }
                $this->info("app migrations cleared");
            }
        }

        //Copy files
        foreach ($module as $m){
            $migrationPath = $path."/".str_replace(".", "/", $m).$pathOption."/";

            if (!file_exists($migrationPath)){
                $this->comment("path to migration does not exists '".$migrationPath."'");
                continue;
            }

            $files = scandir($migrationPath);
            if ($files === false){
                $this->comment("path to migration does not exists '".$migrationPath."'");
                continue;
            }

            $n = 0;
            foreach ($files as $f){
                if (!is_file($migrationPath.$f)){
                    continue;
                }

                $this->line("copy '".$migrationPath.$f."'");
                copy($migrationPath.$f, $appMigrationPath.$f);
                $n++;
            }
            $this->info($n." migrations copied for module ".$m);
        }
    }
}
