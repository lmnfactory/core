<?php

namespace Lmn\Core\Command;

use Illuminate\Console\Command;

class LmnTestCopyCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lmn:test-copy {--clear} {--path=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy test files from modules to test folder.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appMigrationPath = "tests/modules/";
        $path = config("module.path", false);
        $module = config("module.module", false);

        if ($module === false){
            $this->error("module.php config does not contain module property.");
        }
        if ($path === false){
            $this->error("module.php config does not contain path property.");
        }

        $pathOption = $this->option("path");
        if ($pathOption == null){
            $pathOption = "/Build/Test";
        }

        if (!file_exists($appMigrationPath)) {
            mkdir($appMigrationPath, 0755);
        }

        //Clear folder
        if ($this->option("clear")){
            $files = scandir($appMigrationPath);
            if ($files !== false){
                foreach($files as $file){
                    if (!is_dir($appMigrationPath.$file) || $file == ".." || $file == ".") {
                        continue;
                    }
                    $moduleTestFiles = scandir($appMigrationPath.$file);
                    if ($moduleTestFiles !== false){
                        foreach ($moduleTestFiles as $moduleFile) {
                            if(is_file($appMigrationPath.$file."/".$moduleFile)){
                                unlink($appMigrationPath.$file."/".$moduleFile);
                            }
                        }
                    }
                }
                $this->info("app tests cleared");
            }
        }

        //Copy files
        foreach ($module as $m){
            $testPath = $path."/".str_replace(".", "/", $m).$pathOption."/";

            if (!file_exists($testPath)){
                $this->comment("path to tests does not exists '".$testPath."'");
                continue;
            }

            $files = scandir($testPath);
            if ($files === false){
                $this->comment("path to migration does not exists '".$testPath."'");
                continue;
            }

            if (!file_exists($appMigrationPath.$m)) {
                mkdir($appMigrationPath.$m, 0755);
            }

            $n = 0;
            foreach ($files as $f){
                if (!is_file($testPath.$f)){
                    continue;
                }

                $this->line("copy '".$testPath.$f."'");
                copy($testPath.$f, $appMigrationPath.$m."/".$f);
                $n++;
            }
            $this->info($n." tests copied for module ".$m);
        }
    }
}
