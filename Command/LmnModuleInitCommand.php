<?php

namespace Lmn\Core\Command;

use Illuminate\Console\Command;
use Lmn\Core\Lib\Facade\Config;

class LmnModuleInitCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lmn:module-init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize modulear system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = "module";
        $path = config_path($file.'.php');

        $config = [
            'path' => 'app/Module',
            'module' => ['lmn.core'],
            'core' => [
                'jwt' => [
                    'secret' => str_random(16),
                    'iss' => 'lmn.faceschool',
                    'exp' => 3600,
                    'aud' => 'lmn.faceschool'
                ]
            ]
        ];
        $current = [];
        if (file_exists($path)){
            $current = include($path);
        }

        foreach ($config as $key => $value) {
            if (!isset($current[$key])){
                $current[$key] = $config[$key];
            }
        }

        $content = "<?php".PHP_EOL.
            PHP_EOL.
            "return ". Config::print($current).";";

        file_put_contents($path, $content);

        $this->info($file.".php config file created. You can configure it now.");
    }
}
