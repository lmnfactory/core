<?php

namespace Lmn\Core\Test;

use TestCase;

use Lmn\Core\Lib\Map\OneToOneSingleton;
use Lmn\Core\Lib\Map\OneToManySingleton;
use Lmn\Core\Exception\KeyNotExistsException;
use Lmn\Core\Exception\KeyExistsException;

class MapTest extends TestCase {

    /**
     * @var InstanceService $instanceService
     */
    private $mapService;

    public function setUp() {
        parent::setUp();
    }

    public function testAddingToOneToManySingleton() {
        $mapService = $this->app->make('mapService');
        $closure1 = function() {
            return 'result1';
        };
        $closure2 = function() {
            return 'result2';
        };

        $map = new OneToManySingleton();
        $map->add('test1', $closure1);
        $map->add('test1', $closure2);

        $this->assertEquals(['result1', 'result2'], $map->get('test1'));
    }

    /**
     * @expectedException Lmn\Core\Exception\KeyNotExistsException
     */
    public function testGettingFromOneToManySingletonIncorrect() {
        $closure = function() {
            return 'result';
        };

        $map = new OneToManySingleton();
        $map->add('test1', $closure);

        $value = $map->get('wrong.key');
    }

    public function testAddingToOneToOneSingleton() {
        $closure = function() {
            return 'result';
        };

        $map = new OneToOneSingleton();
        $map->add('test1', $closure);

        $this->assertEquals('result', $map->get('test1'));
    }

    /**
     * @expectedException Lmn\Core\Exception\KeyExistsException
     */
    public function testAddingToOneToOneSingletonSameKey() {
        $closure = function() {
            return 'result';
        };

        $map = new OneToOneSingleton();
        $map->add('test1', $closure);
        $map->add('test1', $closure);
    }

    /**
     * @expectedException Lmn\Core\Exception\KeyNotExistsException
     */
    public function testGettingFromOneToOneSingletonIncorrect() {
        $closure = function() {
            return 'result';
        };

        $map = new OneToOneSingleton();
        $map->add('test1', $closure);

        $value = $map->get('testing');
    }
}
