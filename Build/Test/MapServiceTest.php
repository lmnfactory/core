<?php

namespace Lmn\Core\Test;

use TestCase;

use Lmn\Core\Lib\Structure\StructureService;
use Lmn\Core\Lib\Map\OneToOneSingleton;
use Lmn\Core\Lib\Map\OneToManySingleton;
use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

class MapServiceTest extends TestCase {

    /**
     * @var InstanceService $instanceService
     */
    private $mapService;

    public function setUp() {
        parent::setUp();
        $this->mapService = $this->app->make('mapService');
    }

    /**
     * @expectedException Lmn\Core\Exception\KeyExistsException
     */
    public function testAddingSameKey() {
        $this->mapService->add('oneToManySingleton', function() {
            return new OneToManySingleton();
        });
    }

    public function testAddingCorrect() {
        $this->mapService->add('oneToMany', function() {
            return new OneToManySingleton();
        });

        $this->assertEquals(new OneToManySingleton(), $this->mapService->make('oneToMany'));
    }

    public function testGettingCorrect() {
        $this->assertEquals(new OneToManySingleton(), $this->mapService->make('oneToManySingleton'));
    }

    /**
     * @expectedException Lmn\Core\Exception\KeyNotExistsException
     */
    public function testGettingNotExistingKey() {
        $map = $this->mapService->make('one');
    }
}
