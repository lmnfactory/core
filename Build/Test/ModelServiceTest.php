<?php

namespace Lmn\Core\Test;

use TestCase;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ModelValidationError;

class ValidationServiceTest extends TestCase {

    /**
     * @var ValidationService
     */
    private $validationService;

    public function setUp() {
        parent::setUp();
        $this->validationService = $this->app->make(ValidationService::class);
    }

    public function testMakingError() {
        $this->assertEquals(new ModelValidationError('error', 0), $this->validationService->makeError('error', 0));
    }

    public function testLaravelErrorToError() {
        $data = [];
        $validator = \Validator::make($data, [
            'value' => 'required'
        ]);

        $this->assertEquals(['value' => $this->validationService->makeError('The value field is required.')], $this->validationService->laravelErrorToError($validator));
    }
}
