<?php

namespace Lmn\Core\Build\Test;

use TestCase;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ModelValidationError;

class CacheControllerTest extends TestCase {

    public function setUp() {
        parent::setUp();
    }

    public function testRequest() {
        $this->post('/app/cache')
            ->assertResponseStatus(200);
    }
}
