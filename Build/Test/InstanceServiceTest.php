<?php

namespace Lmn\Core\Test;

use TestCase;

use Lmn\Core\Lib\Instance\InstanceService;
use Lmn\Core\Lib\Instance\HandleClass;
use Lmn\Core\Lib\Instance\ClassWrapper;
use Lmn\Core\Lib\Instance\HandleFunction;
use Lmn\Core\Lib\Instance\FunctionWrapper;
use Lmn\Core\Lib\Instance\HandleClosure;
use Lmn\Core\Lib\Instance\ClosureWrapper;
use Lmn\Core\Lib\Instance\HandleObject;
use Lmn\Core\Lib\Instance\ObjectWrapper;

class InstanceServiceTest extends TestCase {

    /**
     * @var InstanceService $instanceService
     */
    private $instanceService;

    public function setUp() {
        parent::setUp();
        $this->instanceService = $this->app->make(InstanceService::class);
    }

    public function testWrappingClosure() {

        $closure = function() {
            return 'test';
        };
        $this->assertEquals(new ClosureWrapper($closure), $this->instanceService->wrap($closure));

        $wrapper = $this->instanceService->wrap($closure);
        $this->assertEquals($closure(), $wrapper->make());
    }

    public function testWrappingClass() {

        $this->assertEquals(new ClassWrapper(HandleClass::class), $this->instanceService->wrap(HandleClass::class));

        $wrapper = $this->instanceService->wrap(HandleClass::class);
        $this->assertEquals(new HandleClass(), $wrapper->make());
    }

    public function testWrappingFunction() {

        $this->assertEquals(new FunctionWrapper('app_path'), $this->instanceService->wrap('app_path'));

        $wrapper = $this->instanceService->wrap('app_path');
        $this->assertEquals(app_path(), $wrapper->make());
    }

    public function testWrappingObject() {

        $closure = $this;
        $this->assertEquals(new ObjectWrapper($this), $this->instanceService->wrap($this));

        $wrapper = $this->instanceService->wrap($this);
        $this->assertEquals($this, $wrapper->make());
    }

    public function testWrappingNull() {
        $this->assertEquals(new ObjectWrapper(null), $this->instanceService->wrap(null));

        $wrapper = $this->instanceService->wrap(null);
        $this->assertEquals(null, $wrapper->make());
    }

    public function testSettingDefault() {
        $instanceService = new InstanceService();

        $instanceService->default(new HandleClass());
        $this->assertEquals(new HandleClass(), $instanceService->getDefault());
    }

    public function testReSettingDefault() {
        $this->instanceService->default(new HandleClass());
        $this->assertEquals(new HandleObject(), $this->instanceService->getDefault());
    }
}
