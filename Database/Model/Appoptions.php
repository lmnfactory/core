<?php

namespace Lmn\Core\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Appoptions extends Model {

    protected $table = 'appoptions';

    protected $fillable = ['name', 'value'];
}
