<?php

namespace Lmn\Core\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Cache\CacheService;
use Lmn\Core\Lib\Response\ResponseService;

class CacheController extends Controller {

    public function cache(Request $request, CacheService $cacheService, ResponseService $responseService) {
        $data = $cacheService->cache();

        return $responseService->response($data);
    }
}
