<?php

namespace Lmn\Core\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Search\SearchService;
use Lmn\Core\Lib\Response\ResponseService;

class SearchController extends Controller {

    public function search(Request $request, SearchService $searchService, ResponseService $responseService) {
        $data = $request->json()->all();
        //TODO: validation
        $results = $searchService->search($data['search']);

        $return = [];
        foreach ($results as $r) {
            $return[] = $r->toJson();
        }

        return $responseService->response($return);
    }
}
