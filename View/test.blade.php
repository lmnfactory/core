<html>
  <head>
    <title>Angular QuickStart</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--    <link rel="stylesheet" href="styles.css">-->
    <!-- 1. Load libraries -->
     <!-- Polyfill for older browsers -->
    <script src="assets/vendor/lmn/core/core-dep.js"></script>
    
    <script>
      System.import('app').catch(function(err){ console.error(err); });
    </script>
  </head>
  <!-- 3. Display the application -->
  <body>
    <lmn-app>Loading...</lmn-app>
  </body>
</html>
