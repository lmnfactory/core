<?php

namespace Lmn\Core\Exception;

class ItemInDatabaseException extends \Exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 500, $previous);
    }
}
