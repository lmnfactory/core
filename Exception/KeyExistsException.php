<?php

namespace Lmn\Core\Exception;

class KeyExistsException extends \Exception {

    public function __construct($message, $code = null, $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
