<?php

namespace Lmn\Core\Exception;

class KeyNotExistsException extends \Exception {

    public function __construct($message, $code = null, $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
