<?php

namespace Lmn\Core\Exception;

class CircularGraphException extends \Exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 500, $previous);
    }
}
