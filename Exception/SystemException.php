<?php

namespace Lmn\Core\Exception;

class SystemException extends \Exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 400, $previous);
    }
}
