<?php

namespace Lmn\Core\Provider;

use Illuminate\Support\ServiceProvider;
use Lmn\Core\Lib\Facade\Lmn;
use Illuminate\Support\Facades\Event;
use Lmn\Core\Command\LmnModuleInitCommand;

class LmnServiceProvider extends ServiceProvider{
    /**
     * array of module names that are registered
     *
     * @var String[]
     */
    private $modules = [];

    private function registerCommands() {
        $app = $this->getApp();
        $app['lmn.module.init'] = $app->share(function () {
            return new LmnModuleInitCommand();
        });
        $this->commands('lmn.module.init');
    }

    /**
     * initialize module that are set in app config module file. Initializing module consits of setting view namespace for module, loading config files and loading providers.
     *
     * @return
     */
    public function loadModules(){
        $modules = config('module.module', false);

        if ($modules === false){
            return;
        }

        foreach ($modules as $m){
            $module = Lmn::module($m);

            $module->load();

            $this->modules[] = $module;
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){
        $this->loadModules();

        foreach ($this->modules as $m){
            $m->getProvider()->register($this);
        }

        $this->registerCommands();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        //Run boot method for all modules
        foreach ($this->modules as $m){
            $m->getProvider()->boot($this);
        }

        //Run routing for all modules
        foreach ($this->modules as $m){
            $m->getProvider()->route($this);
        }

        //Run events for all modules
        foreach ($this->modules as $m){
            $m->getProvider()->event($this);
        }
    }

    /**
     * Retrieve app instance
     * @method getApp
     * @return \App [description]
     */
    public function getApp(){
        return $this->app;
    }

    /**
     * Register event listener with Laravel framework
     * @method listen
     * @param  Event $event
     * @param  Listener $listener
     */
    public function listen($event, $listener) {
        Event::listen($event, $listener);
    }

    /**
     * Subscribe to different events with Laravel framework
     * @method subscribe
     * @param  Subscriber    $subscriber
     */
    public function subscribe($subscriber) {
        Event::subscribe($subscriber);
    }
}
