<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lmn\Core\Provider;

/**
 * Description of CoreServiceProvider
 *
 * @author arksys
 */

use Illuminate\Foundation\Application;
use App\Providers\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * services that will be registered
 */
use Lmn\Core\Lib\JWT\JWTService;

class CoreServiceProvider implements ModuleServiceProvider{

    public function register(Application $app) {
        $app->singleton(JWTService::class, function($app){
            return new JWTService($app['request']);
        });
    }

    public function boot(Application $app) {

    }

    public function event(Application $app) {

    }

    public function route(Application $app) {
        Route::group(['namespace' => 'Lmn\\Core\\Controller'], function() {
            Route::any('test','TestController@index');
        });
    }
}
