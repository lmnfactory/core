<?php

namespace Lmn\Core\Lib\Map;

use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

use Lmn\Core\Lib\Instance\InstanceService;

use Lmn\Core\Lib\Map\MapHandlerInterface;

class OneToOne implements MapHandlerInterface{

    private $map;

    public function __construct() {
        $this->map = [];
    }

    /**
     * Add key value pair to map. Key can hold only one value. If you try to add value to a key that exists, method throws KeyExistsException exception.
     * @method add
     * @param  string $key   unique name
     * @param  mixed $value
     * @throws KeyExistsException
     */
    public function add($key, $value) {
        if (isset($this->map[$key])) {
            throw new KeyExistsException("Key '".$key."' is already in use");
        }

        $this->map[$key] = $value;
    }

    /**
     * Rretrieve value according to key. If you try to retrieve value with a key that does not exists, method throws KeyNotExistsException exception.
     * @method get
     * @param  string $key
     * @throws KeyNotExistsException
     * @return mixed[] value
     */
    public function get($key) {
        return $this->map[$key];
    }

    public function has($key) {
        return isset($this->map[$key]);
    }

    public function getAll() {
        return $this->map;
    }
}
