<?php

namespace Lmn\Core\Lib\Map;

use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

use Lmn\Core\Lib\Instance\InstanceService;

use Lmn\Core\Lib\Map\MapHandlerInterface;

class OneToManySingleton implements MapHandlerInterface{

    private $rawMap;
    private $objectMap;

    public function __construct() {
        $this->rawMap = [];
        $this->objectMap = [];
    }

    private function toObject($key) {
        if (!isset($this->rawMap[$key])) {
            throw new KeyNotExistsException("Key '".$key."' does not exists");
        }

        if (!isset($this->objectMap[$key])) {
            $this->objectMap[$key] = [];
            foreach ($this->rawMap[$key] as $raw) {
                if ($raw == null){
                    continue;
                }
                $this->objectMap[$key][] = $raw->make();
            }
        }
    }

    private function toObjectAll() {
        foreach ($this->rawMap as $key => $rawArray) {
            if (!isset($this->objectMap[$key])) {
                $this->objectMap[$key] = [];
                foreach ($rawArray as $raw) {
                    if ($raw == null){
                        continue;
                    }
                    $this->objectMap[$key][] = $raw->make();
                }
            }
        }
    }

    /**
     * Add key value pair to map. Key can hold multiple values
     * @method add
     * @param  string $key   unique name
     * @param  mixed $value
     */
    public function add($key, $value) {
        $instanceService = \App::make(InstanceService::class);

        if (!isset($this->rawMap[$key])) {
            $this->rawMap[$key] = [];
        }

        $this->rawMap[$key][] = $instanceService->wrap($value);
    }

    /**
     * Rretrieve value according to key. If you try to retrieve value with a key that does not exists, method throws KeyNotExistsException exception.
     * @method get
     * @param  string $key
     * @throws KeyNotExistsException
     * @return mixed value
     */
    public function get($key) {
        $this->toObject($key);

        return $this->objectMap[$key];
    }

    public function has($key) {
        return isset($this->rawMap[$key]);
    }

    public function getAll() {
        if (sizeof($this->rawMap) != sizeof($this->objectMap)) {
            $this->toObjectAll();
        }

        return $this->objectMap;
    }
}
