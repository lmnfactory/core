<?php

namespace Lmn\Core\Lib\Map;

use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

interface MapHandlerInterface {
    /**
     * Add key value pair to map
     * @method add
     * @param  string $key   unique name
     * @param  mixed $value
     * @throws KeyExistsException
     */
    public function add($key, $value);
    /**
     * Retrieve value according to key
     * @method get
     * @param  string $key
     * @throws KeyNotExistsException
     * @return mixed      value
     */
    public function get($key);

    public function has($key);

    public function getAll();
}
