<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceHandler;
use Lmn\Core\Lib\Instance\ClassWrapper;

class HandleClass implements InstanceHandler {

    public function __construct() {

    }

    /**
     * Wrap mixed value into ClassWrapper in mixed is string and class name
     * @method wrap
     * @param  mixed $mixed
     * @return ClassWrapper        or false if mixed is not class name
     */
    public function wrap($mixed) {
        if (is_string($mixed) && class_exists($mixed)) {
            return new ClassWrapper($mixed);
        }

        return false;
    }
}
