<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceWrapper;

class ClosureWrapper implements InstanceWrapper {

    private $closure;

    public function __construct($closure) {
        $this->closure = $closure;
    }

    /**
     * Create instance from Closure
     * @method make
     * @return mixed
     */
    public function make() {
        return ($this->closure)();
    }
}
