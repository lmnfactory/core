<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceWrapper;

/**
 * Wrapper for unspecified object instance
 */
class ObjectWrapper implements InstanceWrapper {

    private $object;

    public function __construct($object) {
        $this->object = $object;
    }

    /**
     * Creating instance is just returning initial value
     * @method make
     * @return Object
     */
    public function make() {
        return $this->object;
    }
}
