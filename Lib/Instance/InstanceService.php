<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceHandler;

/**
 * Creating instances out of class name, function name, Closures and so on.
 */
class InstanceService {

    private $map;
    private $defaultHandler;

    public function __construct() {
        $this->map = [];
        $this->defaultHandler = null;
    }

    /**
     * Add new Instance Handler.
     * @method add
     * @param  InstanceHandler $value
     */
    public function add(InstanceHandler $value) {
        $this->map[] = $value;
    }

    /**
     * Set default handler. If handler is already set, this method will not overide default handler.
     * @method default
     * @param  InstanceHandler $value
     */
    public function default(InstanceHandler $value) {
        if ($this->defaultHandler == null) {
            $this->defaultHandler = $value;
        }
    }

    /**
     * Retrieve default handler. Returns null if there is none default handler set.
     * @method getDefault
     * @return InstanceHandler     or null if there is none default handler set
     */
    public function getDefault() {
        return $this->defaultHandler;
    }

    /**
     * Create instance out of class name, function name, closure and so on.
     * @method wrap
     * @param  mixed $mixed variable that can be instanciate
     * @return InstanceWrapper        or null if no suitable wrapper is found
     */
    public function wrap($mixed) {
        $i = null;
        foreach ($this->map as $m) {
            $i = $m->wrap($mixed);
            if ($i !== false) {
                return $i;
            }
        }

        if ($this->defaultHandler != null) {
            return $this->defaultHandler->wrap($mixed);
        }
        return null;
    }

    public function make($mixed) {
        $wrap = false;
        foreach ($this->map as $m) {
            $wrap = $m->wrap($mixed);
            if ($wrap !== false) {
                break;
            }
        }

        if ($wrap === false && $this->defaultHandler != null) {
            $wrap = $this->defaultHandler->wrap($mixed);
        }

        if ($wrap !== false) {
            return $wrap->make();
        }

        return null;
    }
}
