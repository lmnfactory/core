<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceWrapper;

class FunctionWrapper implements InstanceWrapper {

    private $function;

    public function __construct($function) {
        $this->function = $function;
    }

    /**
     * Create instance from function
     * @method make
     * @return mixed
     */
    public function make() {
        return ($this->function)();
    }
}
