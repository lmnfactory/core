<?php

namespace Lmn\Core\Lib\Instance;

/**
 * Creating instance out of mixed value
 */
interface InstanceWrapper {
    /**
     * make instance from mixed value
     * @method make
     * @return Object any kind of object
     */
    public function make();
}
