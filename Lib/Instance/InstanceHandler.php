<?php

namespace Lmn\Core\Lib\Instance;
use Lmn\Core\Lib\Instance\InstanceHandler;

interface InstanceHandler {
    /**
     * Decide if this class should wrap the mixed value and latter create instance.
     * @method wrap
     * @param  Object $mixed
     * @return InstanceHandler
     */
    public function wrap($mixed);
}
