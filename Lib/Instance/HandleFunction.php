<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceHandler;
use Lmn\Core\Lib\Instance\FunctionWrapper;

class HandleFunction implements InstanceHandler {

    public function __construct() {

    }

    /**
     * Wrap mixed value to FunctionWrapper if mixed value is string and function
     * @method wrap
     * @param  Mixed $mixed
     * @return FunctionWrapper or false if mixed is not a function
     */
    public function wrap($mixed) {
        if (is_string($mixed) && function_exists($mixed)) {
            return new FunctionWrapper($mixed);
        }

        return false;
    }
}
