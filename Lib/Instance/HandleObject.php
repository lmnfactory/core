<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceHandler;
use Lmn\Core\Lib\Instance\ObjectWrapper;

class HandleObject implements InstanceHandler {

    public function __construct() {

    }

    /**
     * Wrapp mixed value into ObjectWrapper
     * @method wrap
     * @param  Object $mixed
     * @return ObjectWrapper
     */
    public function wrap($mixed) {
        return new ObjectWrapper($mixed);
    }
}
