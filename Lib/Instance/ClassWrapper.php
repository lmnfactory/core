<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceWrapper;

class ClassWrapper implements InstanceWrapper {

    private $class;

    public function __construct($class) {
        $this->class = $class;
    }

    /**
     * Create instance from class
     * @method make
     * @return mixed
     */
    public function make() {
        return new $this->class();
    }
}
