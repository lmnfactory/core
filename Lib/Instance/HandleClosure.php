<?php

namespace Lmn\Core\Lib\Instance;

use Lmn\Core\Lib\Instance\InstanceHandler;
use Lmn\Core\Lib\Instance\ClosureWrapper;

class HandleClosure implements InstanceHandler {

    public function __construct() {

    }

    /**
     * Wrap mixed value in ClosureWrapper if mixed is object and instance of Closure
     * @method wrap
     * @param  mixed $mixed
     * @return ClosureWrapper        or false if mixed is not instance of CLosure
     */
    public function wrap($mixed) {
        if (is_object($mixed) && ($mixed instanceof \Closure)) {
            return new ClosureWrapper($mixed);
        }

        return false;
    }
}
