<?php

namespace Lmn\Core\Lib\ArrayStructure;

use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

interface ArrayHandlerInterface {
    /**
     * Add value to array
     * @method add
     * @param  mixed $value
     */
    public function add($value);
    /**
     * Retrieve value according to index
     * @method get
     * @param  integer $index
     * @throws OutOfBoundsException
     * @return mixed      value
     */
    public function get($index = 0);

    public function getAll();
}
