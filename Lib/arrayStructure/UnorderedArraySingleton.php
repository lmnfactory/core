<?php

namespace Lmn\Core\Lib\ArrayStructure;

use Lmn\Core\Lib\Instance\InstanceService;

use Lmn\Core\Lib\ArrayStructure\ArrayHandlerInterface;

class UnorderedArraySingleton implements ArrayHandlerInterface {

    private $rawMap;
    private $objectMap;

    public function __construct() {
        $this->rawMap = [];
        $this->objectMap = [];
    }

    private function toObject($index) {
        if (!isset($this->rawMap[$index])) {
            throw new \OutOfBoundsException("Array does not contains index '".$index."'");
        }

        if (!isset($this->objectMap[$index])) {
            $this->objectMap[$index] = $this->rawMap[$index]->make();
        }
    }

    private function toObjectAll() {
        foreach ($this->rawMap as $index => $raw) {
            if (!isset($this->objectMap[$index])) {
                $this->objectMap[$index] = $raw->make();
            }
        }
    }

    /**
     * Add value to array.
     * @method add
     * @param  mixed $value
     * @throws KeyExistsException
     */
    public function add($value) {
        $instanceService = \App::make(InstanceService::class);

        $this->rawMap[] = $instanceService->wrap($value);
    }

    /**
     * Rretrieve value according to index. If you try to retrieve value with index out of range of array, method will throw OutOfBoundsException exception.
     * @method get
     * @param  integer $index
     * @throws OutOfBoundsException
     * @return mixed value
     */
    public function get($index = 0) {
        $this->toObject($index);
        if (!isset($this->objectMap[$index])) {
            throw new \OutOfBoundsException("Array does not contains index '".$index."'");
        }

        return $this->objectMap[$key];
    }

    public function getAll() {
        if (sizeof($this->rawMap) != sizeof($this->objectMap)) {
            $this->toObjectAll();
        }

        return $this->objectMap;
    }
}
