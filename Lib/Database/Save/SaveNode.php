<?php

namespace Lmn\Core\Lib\Database\Save;

use Lmn\Core\Exception\CircularGraphException;

class SaveNode {

    private $model;
    private $connections;
    private $saved;
    private $saving;

    public function __construct($model) {
        $this->model = $model;
        $this->connections = [];
        $this->saved = false;
        $this->saving = false;
    }

    public function getModel() {
        return $this->model;
    }

    public function add($path, $node) {
        $this->connections[$path] = $node;
    }

    public function isSaved() {
        return $this->saved;
    }

    public function isSaving() {
        return $this->saving;
    }

    public function save() {
        $this->saving = true;
        foreach ($this->connections as $path => $node) {
            if (!$node->isSaved()) {
                if ($node->isSaving()) {
                    throw new CircularGraphException("You are trying to save circular model dependencies.");
                }
                $node->save();
            }
            $this->model->$path = $node->getModel()->id;
        }

        $this->model->save();
        $this->saved = true;
    }
}
