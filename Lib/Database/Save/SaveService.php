<?php

namespace Lmn\Core\Lib\Database\Save;

class SaveService {

    public function __construct() {

    }

    public function begin($model = null) {
        return new SaveBuilder($model);
    }
}
