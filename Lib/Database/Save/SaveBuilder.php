<?php

namespace Lmn\Core\Lib\Database\Save;

class SaveBuilder {

    private $nodes;

    public function __construct($model = null) {
        $this->nodes = new \SplObjectStorage();

        if ($model != null) {
            $node = new SaveNode($model);
            $this->nodes->attach($model, $node);
        }
    }

    public function has($child, $parent, $column = null) {
        if ($column == null) {
            $column = $parent->getTable()."_id";
        }

        if ($this->nodes->contains($child)) {
            $childNode = $this->nodes->offsetGet($child);
        }
        else {
            $childNode = new SaveNode($child);
            $this->nodes->attach($child, $childNode);
        }

        if ($this->nodes->contains($parent)) {
            $parentNode = $this->nodes->offsetGet($parent);
        }
        else {
            $parentNode = new SaveNode($parent);
            $this->nodes->attach($parent, $parentNode);
        }

        $childNode->add($column, $parentNode);
        return $this;
    }

    public function save() {
        foreach ($this->nodes as $value) {
            $model = $this->nodes->current();
            $node = $this->nodes->getInfo();
            if ($node->isSaved()) {
                continue;
            }
            $node->save();
        }
    }

    public function transaction() {
        $self = $this;
        \DB::transaction(function() use ($self){
            $self->save();
        });
    }
}
