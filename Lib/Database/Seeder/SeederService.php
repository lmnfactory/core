<?php

namespace Lmn\Core\Lib\Database\Seeder;

class SeederService {

    private $seeders;

    public function __construct() {
        $arrayService = \App::make('arrayService');
        $this->seeders = $arrayService->make('unorderedSingleton');
    }

    public function addSeeder($seeder) {
        $this->seeders->add($seeder);
    }

    public function run() {
        $seeders = $this->seeders->getAll();
        foreach ($seeders as $s) {
            $s->run();
        }
    }
}
