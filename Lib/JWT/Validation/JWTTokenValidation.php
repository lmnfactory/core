<?php

namespace Lmn\Core\Lib\JWT\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class JWTTokenValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'sub' => 'required'
        ];
    }
}
