<?php

namespace Lmn\Core\Lib\JWT;

class JWTSecretMissingException extends \Exception {
    public function __construc($message, $code, $previous){
        parent::__construct($message, $code, $previous);
    }
}
