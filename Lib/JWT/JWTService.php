<?php

namespace Lmn\Core\Lib\JWT;

use Lmn\Core\Lib\Model\ValidationService;
use Firebase\JWT\JWT;

/**
 * @property ValidationService $validationService
 */
class JWTService {

    private $secret;
    private $validationService;

    public function __construct(ValidationService $validationService){
        $this->secret = null;
        $this->validationService = $validationService;
    }

    public function setSecret($secret) {
        $this->secret = $secret;
    }

    public function getSecret() {
        return $this->secret;
    }

    public function getValidSecret() {
        if (!$this->hasSecret()) {
            throw new JWTSecretMissing("You have to set secret in app moudle provider.");
        }
        return $this->getSecret();
    }

    public function hasSecret() {
        return ($this->secret != false && $this->secret != null && $this->secret != "");
    }

    public function decode($token){
        $secret = $this->getValidSecret();

        return JWT::decode($token, $secret, array('HS256'));
    }

    public function createToken($payload){
        $secret = $this->getValidSecret();

        return JWT::encode($payload, $secret);
    }

    public function encode($tokenValues){
        $this->validationService->systemValidate($tokenValues, 'token.jwt');

        return $this->createToken($tokenValues);
    }
}
