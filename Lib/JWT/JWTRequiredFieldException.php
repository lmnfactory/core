<?php

namespace Lmn\Core\Lib\JWT;

class JWTRequiredFieldException extends \Exception {

    public function __construct($message, $code, $previous){
        parent::__construct($message, $code, $previous);
    }
}
