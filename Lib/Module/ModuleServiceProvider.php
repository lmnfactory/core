<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lmn\Core\Lib\Module;

/**
 *
 * @author arksys
 */

use Lmn\Core\Provider\LmnServiceProvider;

interface ModuleServiceProvider {
    /**
     * Method that allows you to register new services. This method is called first and thats why you should not call any other services from this method.
     *
     * @param LmnServiceProvider $provider
     */
    public function register(LmnServiceProvider $provider);

    /**
     * Method that allows you to configure your (or other) registered services. This metod is called after register and that means that all services should be registered.
     *
     * @param LmnServiceProvider $provider
     */
    public function boot(LmnServiceProvider $provider);

    /**
     * Method that allows you to create routes for your module.
     *
     * @param LmnServiceProvider $provider
     */
    public function route(LmnServiceProvider $provider);

    /**
     * Method that allows you to create your own event for your module.
     *
     * @param LmnServiceProvider $provider
     */
    public function event(LmnServiceProvider $provider);
}
