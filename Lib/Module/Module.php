<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lmn\Core\Lib\Module;

/**
 * Description of Module
 *
 * @author arksys
 */

use Lmn\Core\Lib\Facade\Config;
use Lmn\Core\Lib\Module\ModuleServiceProvider;

class Module {

    private $name;
    private $path;
    private $provider;

    public function __construct($name, ModuleServiceProvider $provider) {
        $this->name = $name;
        $this->provider = $provider;

        $this->path = config("module.path", false);
        if ($this->path === false){
            throw new ModulePathNotFoundException("No path value in module.php config file.");
        }
        $this->path = base_path()."/".$this->path."/".$this->nameToPath();
    }

    private function nameToPath(){
        return str_replace(".", "/", $this->name);
    }

    /**
     * Load all config files for module in config folder
     *
     * @param String $module
     */
    private function loadConfig($module){
        Config::registerAll($module, $this->path."/Config/");
    }

    public function load(){
        //add namespace to view for this module
        view()->addNamespace($this->name, $this->path."/View/");

        //load config files for module
        $this->loadConfig($this->name);
    }

    public function getProvider(){
        return $this->provider;
    }
}
