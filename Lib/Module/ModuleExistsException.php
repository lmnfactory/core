<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lmn\Core\Lib\Module;

/**
 * Description of ModuleExistsEsception
 *
 * @author arksys
 */
class ModuleExistsException extends \Exception{
    public function __construct($message = "", $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
