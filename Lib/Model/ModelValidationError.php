<?php

namespace Lmn\Core\Lib\Model;

class ModelValidationError {

    private $message;
    private $code;

    public function __construct($message, $code){
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * Retrieve error message.
     * @method getMessage
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Retrieve error code
     * @method getCode
     * @return integer
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * Export validation error to asociative array. This array has two indexes 'message' and 'code'
     * @method toArray
     * @return array with two indexes 'message' and 'code'
     */
    public function toArray() {
        return [
            'message' => $this->getMessage(),
            'code' => $this->getCode()
        ];
    }
}
