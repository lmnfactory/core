<?php

namespace Lmn\Core\Lib\Model;

/**
 * Exception that should be thrown when validation fails.
 */
class ValidationException extends \Exception {

    public function __construct($message, $previous = null) {
        parent::__construct($message, 422, $previous);
    }
}
