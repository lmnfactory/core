<?php

namespace Lmn\Core\Lib\Model;

interface ModelValidation {
    /**
     * run validation on data.
     * @method validate
     * @param  array   $data data array
     * @return ModelValidationError[]          array of ModelValidationError if there is any error. It there is no error return true.
     */
    public function validate($data);
}
