<?php

namespace Lmn\Core\Lib\Model;

use Lmn\Core\Lib\Structure\StructureService;

use Lmn\Core\Lib\Model\ModelValidationError;
use Lmn\Core\Lib\Model\ModelValidation;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Exception\SystemException;
use Illuminate\Foundation\Application;

class ValidationService {

    const END_ON_FAIL = 0;
    const END_ON_CYCLE_FINISH = 1;

    private $app;
    private $errors;

    public function __construct(Application $app) {
        $this->app = $app;
        $this->errors = [];
    }

    private function getKey($key) {
        return "lmn.core.validation.".$key;
    }

    /**
     * Add validation implementation to list of available validations. $modelName is a name of validation. If the validation is for form, you shoul use form.<form_name> syntax. If the validation is for model, you should use <table_name> as a validation name.
     * @method add
     * @param  string $modelName  validtion name
     * @param  mixed $validation anything that an be instanciated to ModelValidation
     */
    public function add($modelName, $validation) {
        $this->app->singleton($this->getKey($modelName), $validation);
    }

    /**
     * Run validation for data, with a specific validation. If validation fails, this method will throw ValidationException exception. If no exception is thrown, validation run successfuly.
     * @method validate
     * @param  array   $data     data to be validated
     * @param  string   $modelName validation name
     * @param  integer   $mode      mode of validating. You can choose END_ON_FAIL - validation will end on first fail, END_ON_CYCLE_FINISH - validation will run all the validation rules.
     * @return boolean
     */
    public function validate($data, $modelName, $mode = self::END_ON_FAIL) {
        $val = $this->app->make($this->getKey($modelName));
        $this->errors = $val->validate($data);

        return ($this->errors === true || empty($this->errors));
    }

    /**
    * alias for validate
    */
    public function systemValidate($data, $modelName, $mode = self::END_ON_FAIL) {
        return $this->validate($data, $modelName, $mode);
    }

    /**
     * Run validation for data, with a specific validation. If validation fails, this method will throw ValidationException exception. If no exception is thrown, validation run successfuly.
     * @method validate
     * @param  array   $data     data to be validated
     * @param  string   $modelName validation name
     * @param  integer   $mode      mode of validating. You can choose END_ON_FAIL - validation will end on first fail, END_ON_CYCLE_FINISH - validation will run all the validation rules.
     * @throws ValidationException
     */
    public function validateOrFail($data, $modelName, $mode = self::END_ON_FAIL) 
    {
        if (!$this->validate($data, $modelName, $mode)) {
            throw new ValidationException("Data are invalid");
        }
    }

    public function systemValidateOrFail($data, $modelName, $mode = self::END_ON_FAIL)
    {
        if (!$this->systemValidate($data, $modelName, $mode)) {
            throw new SystemException("System validation exception.");
        }
    }

    /**
     * Create an ModelValidationError object from validation message and validation code.
     * @method makeError
     * @param  string    $message validation error message
     * @param  integer   $code    validation error code
     * @return ModelValidationError             [description]
     */
    public function makeError($message, $code = 0) {
        return new ModelValidationError($message, $code);
    }

    /**
     * Transfer laravel error messages to ModelValidationError instances.
     * @method laravelErrorToError
     * @param  Validator              $validator laravel validator instance
     * @return ModelValidationError[]                         array of ModelValidationError
     */
    public function laravelErrorToError($validator) {
        $validationErrors = [];
        $errors = $validator->errors();
        $keys = $errors->keys();
        foreach ($keys as $key) {
            $e = $errors->first($key);
            $validationErrors[$key] = $this->makeError($e);
        }
        
        return $validationErrors;
    }

    /**
     * Retrieve erros from last validation.
     * @method getErrors
     * @return ModelValidationError[] array of ModelValidationError
     */
    public function getErrors() {
        if ($this->errors === true) {
            return [];
        }
        return $this->errors;
    }

    /**
     * Retrieve errors from last validation as a associative array.
     * @method getErrorsArray
     * @return array         each record in array consists of 'message' and 'code' index.
     */
    public function getErrorsArray() {
        $err = [];

        foreach ($this->getErrors() as $key => $e) {
            $err[$key] = $e->toArray();
        }

        return $err;
    }
}
