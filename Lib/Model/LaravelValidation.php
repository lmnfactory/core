<?php

namespace Lmn\Core\Lib\Model;

use Lmn\Core\Lib\Model\ValidationService;

abstract class LaravelValidation implements ModelValidation {

    public function validate($data) {
        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $rules = $this->getRules($data);

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            return  $validationService->laravelErrorToError($validator);
        }

        return true;
    }

    abstract public function getRules($data);
}
