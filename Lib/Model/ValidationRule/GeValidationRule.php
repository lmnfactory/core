<?php

namespace Lmn\Core\Lib\Model\ValidationRule;

use Lmn\Core\Lib\Model\ValidationRule\ValidationRule;

class GeValidationRule implements ValidationRule {

    public function __construct() {

    }

    public function rule($attribute, $value, $parameters, $validator) {
        $compareTo = intval($parameters[0]);
        $intvalue = intval($value);

        return $intvalue >= $compareTo;
    }
}
