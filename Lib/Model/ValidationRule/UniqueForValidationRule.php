<?php

namespace Lmn\Core\Lib\Model\ValidationRule;

use Lmn\Core\Lib\Model\ValidationRule\ValidationRule;

class UniqueForValidationRule implements ValidationRule {

    public function __construct() {

    }

    public function rule($attribute, $value, $parameters, $validator) {
        $data = $validator->getData();
        $tableName = array_shift($parameters);
        $checkColumn = array_shift($parameters);

        $query = \DB::table($tableName)
            ->where($checkColumn, '=', $value);
            
        while (!empty($parameters)) {
            $column = array_shift($parameters);
            if (empty($parameters)) {
                throw \Exception("Unique for validator is missing parameter");
            }
            $index = array_shift($parameters);
            $query->where($column, '=', $data[$index]);
        }

        $result = $query->count();
        
        return ($result == 0);
    }
}
