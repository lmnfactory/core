<?php

namespace Lmn\Core\Lib\Model\ValidationRule;

interface ValidationRule {
    public function rule($attribute, $value, $parameters, $validator);
}
