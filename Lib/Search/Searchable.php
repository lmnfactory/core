<?php

namespace Lmn\Core\Lib\Search;

interface Searchable {
    public function search($search);
}