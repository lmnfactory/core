<?php

namespace Lmn\Core\Lib\Search;

use \Lmn\Core\Lib\Search\SearchResult;

class SearchFactory {

    public function __construct() {
        
    }

    public function result($data, $relevance, $group) {
        return new SearchResult($data, $relevance, $group);
    }
}