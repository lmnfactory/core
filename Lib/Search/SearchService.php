<?php

namespace Lmn\Core\Lib\Search;

class SearchService {
    
    private $searchables;

    public function __construct() {
        $this->searchables = [];
    }

    private function limit() {

    }

    private function order() {

    }

    public function add($searchable) {
        $this->searchables[] = $searchable;
    }

    public function search($search) {
        $relevance = [];
        foreach ($this->searchables as $s) {
            $relevance = \array_merge($relevance, $s->search($search));
        }

        \usort($relevance, function($a, $b) {
            if ($a->getRelevance() > $b->getRelevance()) {
                return 1;
            }
            else if ($a->getRelevance() == $b->getRelevance()) {
                return 0;
            }
            else {
                return -1;
            }
        });

        // $relevance = $this->limit();
        // $relevance = $this->order();

        return $relevance;
    }
}