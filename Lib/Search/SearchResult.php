<?php

namespace Lmn\Core\Lib\Search;

class SearchResult {

    private $data;
    private $relevance;
    private $group;

    public function __construct($data, $relevance, $group) {
        $this->data = $data;
        $this->relevance = $relevance;
        $this->group = $group;
    }

    public function getRelevance() {
        return $this->relevance;
    }

    public function toJson() {
        return [
            'data' => $this->data,
            'relevance' => $this->getRelevance(),
            'group' => $this->group
        ];
    }
}