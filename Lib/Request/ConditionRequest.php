<?php

namespace Lmn\Core\Lib\Request;

use Illuminate\Http\Request;
use Lmn\Core\Lib\Database\Select\ConditionCollectionService;
use Lmn\Core\Lib\Variable\VariableService;

class ConditionRequest {

    private $request;
    private $conditionCollection;

    public function __construct(Request $request) {
        /** @var ConditionCollectionService $conditionCollectionService */
        $conditionCollectionService = \App::make(ConditionCollectionService::class);

        $this->request = $request;
        $this->conditionCollection = $conditionCollectionService->create();

        $this->init();
    }

    private function addFilter($filterData = null) {
        if ($filterData == null) {
            return;
        }

        $filter = $this->conditionCollection->getFilter();
        foreach ($filterData as $key => $value) {
            $filter->add($key, $value);
        }
    }

    private function addOrder($orderData = null) {
        if ($orderData == null) {
            return;
        }

        $order = $this->conditionCollection->getOrder();
        foreach ($orderData as $key => $value) {
            $order->add($key, $value);
        }
    }

    private function init() {
        $filterData = $this->request->json('option.filter');
        $orderData = $this->request->json('option.order');

        $this->addFilter($filterData);
        $this->addOrder($orderData);
    }

    public function getConditionCollection() {
        return $this->conditionCollection;
    }

    public function getFilter($key = null, $type = null) {
        if ($key == null){
            return $this->conditionCollection->getFilter();
        }

        $filterValue = $this->conditionCollection->getFilter()->get($key);

        if ($type != null) {
            $varService = \App::make(VariableService::class);
            $filterValue = $varService->convert($filterValue, $type);
        }

        return $filterValue;
    }

    public function getOrder() {
        return $this->conditionCollection->getOrder();
    }

    public function getRequest() {
        return $this->request;
    }
}
