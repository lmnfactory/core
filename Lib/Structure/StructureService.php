<?php

namespace Lmn\Core\Lib\Structure;

use Lmn\Core\Exception\KeyExistsException;
use Lmn\Core\Exception\KeyNotExistsException;

class StructureService {

    private $callbacks;

    public function __construct() {
        $this->callbacks = [];
    }

    /**
     * Add implementation of map.
     * @method add
     * @param  string $key      map name
     * @param  Closure $callback function that returns desired instance instance
     * @throws KeyExistsException
     */
    public function add($key, $callback) {
        if (isset($this->callbacks[$key])) {
            throw new KeyExistsException("Key '".$key."' is already in use");
        }

        $this->callbacks[$key] = $callback;
    }

    /**
     * Retrieve new instance of map implementation.
     * @method make
     * @param  string $key map name
     * @throws KeyNotExistsException
     * @return Object      type of your desired instance
     */
    public function make($key) {
        if (!isset($this->callbacks[$key])) {
            throw new KeyNotExistsException("Key '".$key."' does not exists");
        }

        return $this->callbacks[$key]();
    }
}
