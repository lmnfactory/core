<?php

namespace Lmn\Core\Lib\Cache;

use Lmn\Core\Lib\Structure\StructureService;
use Lmn\Core\Lib\Map\MapHandlerInterface;
use Lmn\Core\Lib\Cache\Cacheable;

class CacheService {

    /**
     * @var Lmn\Core\Lib\Map\MapHandlerInterface $map
     */
    private $map;

    public function __construct() {
        /**
         * @var Lmn\Core\Lib\Structure\StructureService $mapService
         */
        $mapService = \App::make('mapService');
        $this->map = $mapService->make('oneToOneSingleton');
    }

    /**
     * Add another cachable class, that want to send user localy cached data.
     * @method add
     * @param  string $key      unique name
     * @param  mixed $cachable Anything Instanceable - Closure, class name, instance, ...
     */
    public function add($key, $cachable) {
        $this->map->add($key, $cachable);
    }

    /**
     * Retrieve Cacheable instance by $key index.
     * @method get
     * @param  string $key unique name
     * @return Cacheable
     */
    public function get($key) {
        return $this->map->get($key);
    }

    /**
     * Retrieve all cache data from all added cacheable classes. Data are index according to indexes cacheable classes were added by 'add' method.
     * @method cache
     * @return array
     */
    public function cache() {
        $cache = [];
        $all = $this->map->getAll();
        foreach ($all as $key => $value) {
            $cache[$key] = $value->cache();
        }
        return $cache;
    }
}
