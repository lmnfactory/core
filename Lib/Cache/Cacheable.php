<?php

namespace Lmn\Core\Lib\Cache;

interface Cacheable {
    /**
     * Retrieve data that can user cache localy. It means those data doesn't change often.
     * @method cache
     * @return array list/data for user to cache localy.
     */
    public function cache();
}
