<?php

namespace Lmn\Core\Lib\Cache;

use Lmn\Core\Lib\Cache\Cacheable;

class TableCache implements Cacheable {

    private $tableName;

    public function __construct($tableName) {
        $this->tableName = $tableName;
    }

    public function cache() {
        return \DB::table($this->tableName)->get();
    }
}
