<?php

namespace Lmn\Core\Lib\Repository\Config;

interface RepositoryConfig {
    public function set($key, $value);
    public function get($key);
    public function has($key);
    public function push($key, $value);
}
