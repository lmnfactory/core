<?php

namespace Lmn\Core\Lib\Repository\Config;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;

abstract class AbstractRepositoryConfig implements RepositoryConfig {

    private $_config;

    public function __construct() {
        $this->_config = [];
    }

    public function get($key, $default = null) {
        return array_get($this->_config, $key, $default);
    }

    public function set($key, $value) {
        array_set($this->_config, $key, $value);
    }

    public function has($key) {
        return array_has($this->_config, $key);
    }

    public function push($key, $value) {
        if (!$this->has($key)) {
            $this->set($key, []);
        }
        $arr = $this->get($key);
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $arr[$k] = $v;
            }
        }
        else {
            $arr[] = $value;
        }
        $this->set($key, $arr);
    }
}
