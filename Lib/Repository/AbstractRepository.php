<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\Repository;

abstract class AbstractRepository implements Repository {

    public function __construct() {

    }

    public function get() {

    }

    public function first() {
        return $this->get();
    }

    public function all() {

    }

    public function exists() {

    }

    public function delete() {

    }

    public function create($data) {

    }

    public function update($data) {

    }

    public function debug() {

    }
}
