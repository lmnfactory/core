<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\ListenerEloquentRepository;

abstract class AbstractListenerEloquentRepository implements ListenerEloquentRepository {

    public function onCreate($model, $data) {

    }

    public function onUpdate($model, $data) {

    }

    public function onGet($model) {

    }

    public function onList($models) {

    }

    public function onDelete($model) {

    }
}
