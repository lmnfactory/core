<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\AbstractRepository;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

abstract class AbstractEloquentRepository extends AbstractRepository implements EloquentRepository {

    protected $_criteria;
    protected $_permission;
    protected $criteriaService;

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct();
        $this->criteriaService = $criteriaService;
        $this->_criteria = [];
    }

    private function query() {
        $model = $this->getModel();
        return $model::query();
    }

    private function applyCriteria($query) {
        foreach ($this->_criteria as $c) {
            $c->apply($query);
        }
    }

    public function clear() {
        $this->_criteria = [];
        return $this;
    }

    public function criteria($criteria, $args = []) {
        $this->_criteria[] = $this->criteriaService->get($criteria, $args);
        return $this;
    }

    public function permission($permission) {
        $this->_permission = $permission;
        return $this;
    }

    public function getPermission() {
        return $this->_permission;
    }

    abstract public function getModel();

    public function get() {
        $query = $this->query();
        $this->applyCriteria($query);
        return $query->first();
    }

    public function all() {
        $query = $this->query();
        $this->applyCriteria($query);
        return $query->get();
    }

    public function create($data) {
        $model = $this->getModel();
        return $model::create($data);
    }

    public function update($data) {
        $model = $this->getModel();
        $instance = new $model();
        $query = $this->query();
        $this->applyCriteria($query);

        $fillable = $instance->getFillable();
        $updateData = [];
        foreach ($fillable as $f) {
            if (!array_key_exists($f, $data)) {
                continue;
            }
            if (isset($data[$f])) {
                $updateData[$f] = $data[$f];
            } else {
                $updateData[$f] = null;
            }
        }

        $query->update($updateData);
        return $this->get();
    }

    public function delete() {
        $query = $this->query();
        $this->applyCriteria($query);
        return $query->delete();
    }

    public function debug() {
        $query = $this->query();
        $this->applyCriteria($query);
        return $query->toSql();
    }
}
