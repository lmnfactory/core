<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;

class BasicListener extends AbstractListenerEloquentRepository {

    private $repo;
    private $foreignKey;
    private $key;

    public function __construct($key, EloquentRepository $repo, $foreignKey) {
        $this->key = $key;
        $this->repo = $repo;
        $this->foreignKey = $foreignKey;
    }

    private function prepareRepo($model) {
        $this->repo->clear()
            ->criteria('core.eq', ['name' => $this->foreignKey, 'value' => $model->id]);
    }

    public function onCreate($model, $data) {
        $data[$this->foreignKey] = $model->id;
        $this->prepareRepo($model);
        return $this->repo->create($data);
    }

    public function onUpdate($model, $data) {
        $data[$this->foreignKey] = $model->id;
        $this->prepareRepo($model);
        return $this->repo->update($data);
    }

    public function onGet($model) {
        $key = $this->key;
        $this->prepareRepo($model);
        $model->$key = $this->repo->get();
    }

    public function onList($models) {
        $key = $this->key;
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $list = $this->repo->clear()
            ->criteria('core.in', ['name' => $this->foreignKey, 'value' => $ids])
            ->all();

        $fk = $this->foreignKey;
        $listSorted = [];
        foreach ($list as $l) {
            if (!isset($listSorted[$l->$fk])) {
                $listSorted[$l->$fk] = [];
            }
            $listSorted[$l->$fk][] = $l;
        }
        foreach ($models as $model) {
            if (!isset($listSorted[$model->id])) {
                $model->$key = [];
                continue;
            }
            $model->$key = $listSorted[$model->id];
        }
    }

    public function onDelete($model) {
        $this->prepareRepo($model);
        return $this->repo->delete();
    }
}
