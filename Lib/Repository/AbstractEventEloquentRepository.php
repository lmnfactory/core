<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\AbstractRepository;
use Lmn\Core\Lib\Repository\ListenerEloquentRepository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

abstract class AbstractEventEloquentRepository extends AbstractEloquentRepository {

    private $_events;

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
        $this->events = [];
    }

    public function on($events, ListenerEloquentRepository $repo) {
        if ($events == "*") {
            $events = ['create', 'update', 'get', 'list', 'delete'];
        }
        else if (!is_array($events)) {
            $events = [$events];
        }

        foreach ($events as $event) {
            if (!isset($this->_events[$event])) {
                $this->_events[$event] = [];
            }
            $this->_events[$event][] = $repo;
        }
    }

    private function getEvent($event) {
        if (!isset($this->_events[$event])) {
            return [];
        }
        return $this->_events[$event];
    }

    public function onCreate($model, $data) {
        $extensions = $this->getEvent('create');
        foreach ($extensions as $ex) {
            $ex->onCreate($model, $data);
        }
    }

    public function onUpdate($model, $data) {
        $extensions = $this->getEvent('update');
        foreach ($extensions as $ex) {
            $ex->onUpdate($model, $data);
        }
    }

    public function onGet($model) {
        $extensions = $this->getEvent('get');
        foreach ($extensions as $ex) {
            $ex->onGet($model);
        }
    }

    public function onList($models) {
        $extensions = $this->getEvent('list');
        foreach ($extensions as $ex) {
            $ex->onList($models);
        }
    }

    public function onDelete($model) {
        $extensions = $this->getEvent('delete');
        foreach ($extensions as $ex) {
            $ex->onDelete($model);
        }
    }

    public function get() {
        $model = parent::get();
        if ($model != null) {
            $this->onGet($model);
        }
        return $model;
    }

    public function all() {
        $list = parent::all();
        $this->onList($list);
        return $list;
    }

    public function create($data) {
        $model = parent::create($data);
        if ($model != null) {
            $this->onCreate($model, $data);
        }
        return $model;
    }

    public function update($data) {
        $model = parent::update($data);
        if ($model != null) {
            $this->onUpdate($model, $data);
        }
        return $model;
    }

    public function delete() {
        return parent::delete();
    }
}
