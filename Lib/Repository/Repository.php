<?php

namespace Lmn\Core\Lib\Repository;

interface Repository {
    public function get();
    public function first();
    public function all();
    public function exists();
    public function update($data);
    public function create($data);
    public function delete();
    public function debug();
}
