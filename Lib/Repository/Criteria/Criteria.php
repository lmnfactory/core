<?php

namespace Lmn\Core\Lib\Repository\Criteria;
use Illuminate\Database\Eloquent\Builder;

interface Criteria {
    public function set($args);
    public function apply(Builder $builder);
}
