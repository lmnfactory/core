<?php

namespace Lmn\Core\Lib\Repository\Criteria;

interface CriteriaService {
    public function add($key, $criteria);
    public function get($key, $args = []);
}
