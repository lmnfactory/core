<?php

namespace Lmn\Core\Lib\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Instance\InstanceService;
use Lmn\Core\Lib\Map\MapHandlerInterface;
use Illuminate\Foundation\Application;

/**
 * @property Application $app
 */
class EloquentCriteriaService implements CriteriaService {

    private $app;

    public function __construct(Application $app) {
        $this->app = $app;
    }

    private function getKey($key) {
        return "lmn.core.criteria.".$key;
    }

    public function add($key, $criteria) {
        $this->app->bind($this->getKey($key), $criteria);
    }

    public function get($key, $args = []) {
        $criteria = $this->app->make($this->getKey($key));
        $criteria->set($args);
        return $criteria;
    }
}
