<?php

namespace Lmn\Core\Lib\Repository;

interface ListenerEloquentRepository {
    public function onGet($model);
    public function onList($models);
    public function onCreate($model, $data);
    public function onUpdate($model, $data);
    public function onDelete($model);
}
