<?php

namespace Lmn\Core\Lib\Repository;

use Lmn\Core\Lib\Repository\AbstractRepository;
use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

abstract class AbstractEloquentRepositoryExtension extends AbstractRepository implements EloquentRepository {

    private $repo;

    public function __construct(EloquentRepository $repo) {
        parent::__construct();
        $this->repo = $repo;
    }

    public function clear() {
        $this->repo->clear();
        return $this;
    }

    public function criteria($criteria, $args = []) {
        $this->repo->criteria($criteria, $args);
        return $this;
    }

    public function permission($permission) {
        $this->repo->permission($permission);
        return $this;
    }

    public function getPermission() {
        return $this->repo->getPermission();
    }

    public function getModel() {
        return $this->repo->getModel();
    }

    public function get() {
        return $this->repo->get();
    }

    public function all() {
        return $this->repo->all();
    }

    public function create($data) {
        return $this->repo->create($data);
    }

    public function update($data) {
        return $this->repo->update($data);
    }

    public function delete() {
        return $this->repo->delete();
    }

    public function debug() {
        return $this->repo->debug();
    }
}
