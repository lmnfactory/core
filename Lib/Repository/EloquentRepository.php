<?php

namespace Lmn\Core\Lib\Repository;

interface EloquentRepository extends Repository {
    public function clear();
    public function criteria($criteria, $args = []);
    public function permission($permission);
    public function getPermission();
}
