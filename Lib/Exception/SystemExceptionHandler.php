<?php

namespace Lmn\Core\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Exception\SystemException;
use Lmn\Core\Lib\Response\ResponseService;

class SystemExceptionHandler implements ExceptionHandler {

    public function __construct() {

    }

    public function report(\Exception $ex) {

    }

    public function render($request, \Exception $ex, ResponseService $responseService) {
        return $responseService->response($ex->getMessage(), $ex->getCode());
    }
}
