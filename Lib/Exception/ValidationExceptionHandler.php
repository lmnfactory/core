<?php

namespace Lmn\Core\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Response\ResponseService;

class ValidationExceptionHandler implements ExceptionHandler {

    public function __construct() {

    }

    public function report(\Exception $ex) {

    }

    public function render($request, \Exception $ex, ResponseService $responseService) {
        $validationService = \App::make(ValidationService::class);
        return $responseService->response($validationService->getErrorsArray(), $ex->getCode());
    }
}
