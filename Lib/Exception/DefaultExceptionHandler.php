<?php

namespace Lmn\Core\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Lib\Response\ResponseService;

class DefaultExceptionHandler implements ExceptionHandler {

    public function __construct() {

    }

    public function report(\Exception $ex) {

    }

    public function render($request, \Exception $ex, ResponseService $responseService) {
        $debug = config('app.debug', false);
        if ($debug) {
            $message = $responseService->createMessage($ex->getMessage(), 500);
            $message->setOption($ex->getTrace());
        }
        else {
            $message = $responseService->createMessage("Something went wrong.", 500);
        }
        return $responseService->send($message);
    }
}
