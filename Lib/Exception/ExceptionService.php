<?php

namespace Lmn\Core\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Exception\KeyExistsException;

/**
 * @property ResponseService $responseService
 */
class ExceptionService {

    private $handlers;
    private $responseService;
    private $dontReport;

    public function __construct(ResponseService $responseService) {
        $this->handlers = [];
        $this->defaultHandler = null;
        $this->responseService = $responseService;
        $this->dontReport = [];
    }

    public function addDontReport($dontReport)
    {
        $this->dontReport = array_merge($this->dontReport, $dontReport);
    }

    public function getDontReport()
    {
        return $this->dontReport;
    }

    public function add($exceptionName, ExceptionHandler $handler) {
        if ($exceptionName == \Exception::class) {
            return $this->default($handler);
        }

        if (isset($this->handlers[$exceptionName])) {
            throw new KeyExistsException("Exception '".$exceptionName."' already has handler.");
        }
        $this->handlers[$exceptionName] = $handler;
        return $this;
    }

    public function default(ExceptionHandler $handler) {
        if ($this->defaultHandler == null) {
            $this->defaultHandler = $handler;
        }
        return $this;
    }

    public function getDefault() {
        return $this->defaultHandler;
    }

    public function hasDefault() {
        return ($this->defaultHandler != null);
    }

    public function report(\Exception $ex) {
        $handled = false;
        foreach ($this->handlers as $exClass => $h) {
            if (!is_a($ex, $exClass)) {
                continue;
            }

            $h->report($ex);
            $handled = true;
            break;
        }

        if (!$handled && $this->hasDefault()) {
            $this->defaultHandler->report($ex);
        }
    }

    public function render($request, \Exception $ex) {
        $response = false;
        foreach ($this->handlers as $exClass => $h) {
            if (!is_a($ex, $exClass)) {
                continue;
            }

            $response = $h->render($request, $ex, $this->responseService);
            if ($response !== false) {
                return $response;
            }
        }

        if ($this->hasDefault()) {
            $response = $this->defaultHandler->render($request, $ex, $this->responseService);
        }

        return $response;
    }
}
