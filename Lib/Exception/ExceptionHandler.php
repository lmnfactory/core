<?php

namespace Lmn\Core\Lib\Exception;

use Lmn\Core\Lib\Response\ResponseService;

interface ExceptionHandler {
    public function report(\Exception $ex);
    public function render($request, \Exception $ex, ResponseService $responseService);
}
