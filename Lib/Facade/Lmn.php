<?php

namespace Lmn\Core\Lib\Facade;

use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Lmn\Core\Lib\Module\ModuleNotFoundException;
use Lmn\Core\Lib\Module\ModuleExistsException;
use Lmn\Core\Lib\Module\ModuleConfigNotFoundException;
use Lmn\Core\Lib\Module\ModuleWrongConfigException;
use Lmn\Core\Lib\Module\Module;

class Lmn {
    public static $modules = [];

    /**
     * Create module from module name. You have to have correct module.php config file inside laravel framework. To generate config.php file run 'php artisan lmn:module-init' command.
     * @method createModule
     * @param  string       $module module name
     * @throws ModulePathNotFoundException
     * @throws ModuleConfigNotFoundException
     * @throws ModuleWrongConfigException
     * @return Module
     */
    private static function createModule($module){
        $path = config("module.path", false);
        if ($path === false){
            throw new ModulePathNotFoundException("No path value in module.php config file.");
        }
        $path = base_path()."/".$path."/";

        $modulePath = $path.str_replace(".", "/", $module);
        $moduleConfigPath = $modulePath."/Config/module.php";
        if (!file_exists($moduleConfigPath)){
            throw new ModuleConfigNotFoundException("No module.php config file for module '".$module."' (".$moduleConfigPath.")");
        }

        $moduleConfig = include($moduleConfigPath);

        if ($moduleConfig == null || !is_array($moduleConfig) || !isset($moduleConfig['name']) || !isset($moduleConfig['provider']) || $moduleConfig['name'] != $module){
            if ($moduleConfig == null){
                throw new ModuleWrongConfigException("module.php config file of module '".$module."' does not contains any values.");
            }
            else if (!is_array($moduleConfig)){
                throw new ModuleWrongConfigException("module.php config file of module '".$module."' does not returns array.");
            }
            else if (!isset($moduleConfig['name'])){
                throw new ModuleWrongConfigException("module.php config file of module '".$module."' does not contains name value.");
            }
            else if (!isset($moduleConfig['provider'])){
                throw new ModuleWrongConfigException("module.php config file of module '".$module."' does not contains provider value.");
            }
            else if ($moduleConfig['name'] != $module){
                throw new ModuleWrongConfigException("module.php config file of module '".$module."' name does not match with config name value.");
            }
        }

         return new Module($module, new $moduleConfig['provider']());
    }

    /**
     * Retreive module by $name. If module wasn't initiated yet, this method will try to initiate it. If it cannot initiate it, it will throw error.
     * @method module
     * @param  string $name module name
     * @throws ModuleNotFoundException
     * @return Module       [description]
     */
    public static function module($name){
        if (!isset(self::$modules[$name])){
            self::$modules[$name] = self::createModule($name);
        }

        if (!isset(self::$modules[$name])){
            throw new ModuleNotFoundException("Module '".$name."' not found.");
        }

        return self::$modules[$name];
    }
}
