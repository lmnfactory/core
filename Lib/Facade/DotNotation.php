<?php

namespace Lmn\Core\Lib\Facade;

class DotNotation {

    /**
     * Takes an input string, checks if it's empty,
     * if not it will check whether it has '.' at the end, if not it will add '.' at the end.
     * @param String $prefix string being transferd into prefix
     * @return String either an ampty string or string with a '.' at the end
     */
    public function getPrefix($prefix){
        if ($prefix != "" && substr($prefix, -1) != ".")
        {
            $prefix = $prefix.".";
        }
        return $prefix;
    }

    /**
     * Take value and split it into index sequence by '.'.
     * @param: string|array $value
     * @return: array
     */
    public static function parse($value)
    {
        if ($value == null || $value == "")
        {
            return array();
        }

        if (is_array($value))
        {
            return $value;
        }

        $dot = explode('.', $value);
        return $dot;
    }

    public static function set(&$array, $indexDot, $value){
        $dot = DotNotation::parse($indexDot);
        $len = sizeof($dot);
        for ($i = 0; $i < $len; $i++){
            if ($i + 1 < $len){
                if (!isset($array[$dot[$i]]))
                {
                    $array[$dot[$i]] = array();

                }
                $array = &$array[$dot[$i]];
            }
            else{
                $array[$dot[$i]] = $value;
            }
        }
    }

    public function assign($array, $indexDot, $value){
        $array = $this->create($array, $indexDot);
        //$array
    }


    /**
     * This function takes two parameters, reference to an array, and array index dot notation.
     * Function tries to find position in array acording to indexDot value, if it fails,
     * it will create position in array and return reference to that position.
     *
     * @param: array &$array, String|array $indexDot
     * @return: &array
     */
    public function create(&$array, $indexDot)
    {
        $dot = DotNotation::parse($indexDot);

        foreach ($dot as $i)
        {
            if (!isset($array[$i]))
            {
                $array[$i] = array();

            }
            $array = &$array[$i];
        }
        return $array;
    }

    /**
     * This function takes two parameters, reference to an array, and array index dot notation.
     * Function tries to find position in array acording to indexDot value, if it fails,
     * it will return null, otherwise, it returns a reference to that position in array.
     *
     * @param: array &$array, String|array $indexDot
     * @return: &array may be null
     */
    public static function find($array, $indexDot)
    {
        $dot = DotNotation::parse($indexDot);

        foreach ($dot as $i)
        {
            if (!isset($array[$i]))
            {
                return null;

            }
            $array = &$array[$i];
        }
        return $array;
    }

    public function exists($array, $indexDot)
    {
        $dot = DotNotation::parse($indexDot);

        foreach ($dot as $i)
        {
            if (!isset($array[$i]))
            {
                return false;

            }
            $array = $array[$i];
        }
        return true;
    }
}
