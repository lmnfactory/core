<?php

namespace Lmn\Core\Lib\Facade;

use Lmn\Core\Lib\Facade\DotNotation;

class Config {

    private static $config = [];

    /**
     * Process all config files inside module folder. Processed files can be latter retrieved by calling `get` method.
     * @method registerAll
     * @param  string      $module module name
     * @param  string      $folder path to folder
     */
    public static function registerAll($module, $folder){
        $files = glob($folder.'*.{php}', GLOB_BRACE);
        foreach($files as $file) {
            self::register($module, $file);
        }
    }

    /**
     * Process one config file. Retrieve all data from config file and store it in array.
     * @method register
     * @param  string   $module module name
     * @param  string   $file   path to file
     */
    public static function register($module, $file){
        $name = basename($file, '.php');
        $fileContent = include($file);
        array_set(self::$config, $module.".".$name, $fileContent);
    }

    /**
     * Retrieve data from module processed config files.
     * @method get
     * @param  string  $config  data to retrieve. you canuse dot notation to access specific data.
     * @param  boolean $default default value to be returned if specified data are not found in config.
     * @return mixed
     */
    public static function get($config, $default = false){
        return array_get(self::$config, $config, $default);
    }

    /**
     * use laravel config. This method calls laravel config helper function.
     * @method app
     * @param  string  $config  config index
     * @param  boolean $default default value to be returned if specified data are not found in config.
     * @return mixed
     */
    public static function app($config, $default = false){
        return config($config, $default);
    }

    /**
     * Transfor asociative array to string php array structure.
     * @method print
     * @param  array  $data  data to transform
     * @param  integer $level dimension of current array
     * @return string
     */
    public static function print($data, $level = 1){
        $ret = "";
        if (is_array($data)){
            $size = sizeof($data);
            $j = 0;
            $ret .= "[".PHP_EOL;
            foreach ($data as $key => $val){
                for ($i = 0; $i < $level; $i++){
                    $ret .= "   ";
                }

                $endline = PHP_EOL;
                if ($j + 1 < $size){
                    $endline = ",".$endline;
                }

                if (is_string($key)){
                    $ret .= "'".$key."' => ".self::print($val, $level + 1).$endline;
                }
                else{
                    $ret .= self::print($val, $level + 1).$endline;
                }

                $j++;
            }

            for ($i = 0; $i < $level - 1; $i++){
                $ret .= "   ";
            }
            $ret .= "]";
        }
        else{
            return "'".$data."'";
        }

        return $ret;
    }
}
