<?php

namespace Lmn\Core\Lib\Facade;

class Generator {

    public static function uniqueString($tableName, $column, $size = 32) {
        $id = false;
        $item = null;
        while ($item !== null || $id === false){
            $id = str_random($size);
            $item = \DB::table($tableName)->where($column, '=', $id)->first();
        }

        return $id;
    }
}
