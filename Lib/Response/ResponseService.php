<?php

namespace Lmn\Core\Lib\Response;

use Lmn\Core\Lib\Response\ResponseMessage;
use Lmn\Core\Exception\KeyNotExistsException;

class ResponseService {

    private $preparedMessages;

    public function __construct(){
        $this->onResponseCallbacks = [];
        $this->preparedMessages = [];
    }

    public function prepare($key, $message)
    {
        $this->preparedMessages[$key] = $message;
    }

    /**
     * Create response message with data and option value.
     * @method createMessage
     * @param  mixed        $data   message data. Data that user requested
     * @param  mixed        $option option data. Data that user did not directly requested, but needs them.
     * @return ResponseMessage messsage instance
     */
    public function createMessage($data, $code = 200) {
        return new ResponseMessage($data, $code);
    }

    /**
     * Create response message with data and opton values and prepare this message to array form. You can just return this value from controller method. Response method triggers ResponseEvent event.
     * @method response
     * @param  mixed   $data   message data. Data that user requested
     * @param  integer   $code http response status code
     * @event ResponseEvent fires response event
     * @return array message transformed into array
     */
    public function response($data, $code = 200){
        return $this->send($this->createMessage($data, $code));
    }

    /**
     * Prepare message to array form. You can just return this value from controller method. Response method triggers ResponseEvent event.
     * @method send
     * @param  ResponseMessage $message message
     * @event ResponseEvent fires response event
     * @return array                   message transformed into array
     */
    public function send(ResponseMessage $message){
        return response()->json($message->toArray(), $message->getCode());
    }

    public function use($name)
    {
        if (!isset($this->preparedMessages[$name])) {
            throw new KeyNotExistsException("response message with the name '" . $name . "' does not exists");
        }
        return $this->send($this->preparedMessages[$name]);
    }
}
