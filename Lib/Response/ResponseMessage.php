<?php

namespace Lmn\Core\Lib\Response;

class ResponseMessage {

    private $data;
    private $option;
    private $header;
    private $code;

    /**
     * Create response message and set data and option values
     * @method __construct
     * @param  mixed      $data   message data. Data that user requested
     * @param  integer      $code http resposne code number
     */
    public function __construct($data, $code = 200){
        $this->setData($data)
            ->setCode($code)
            ->setOption([])
            ->setHeader([]);
    }

    /**
     * Set data (body) part of message. All previous data will be removed by this new data value.
     * @method setData
     * @param  [type]mixed  $data data that user requested.
     * @return ResponseMessage
     */
    public function setData($data){
        $this->data = $data;
        return $this;
    }

    /**
     * Retrieve message data
     * @method getData
     * @return mixed
     */
    public function getData(){
        return $this->data;
    }

    /**
     * Set option part of message. All previous option data will be replaced by this $option data.
     * @method setOption
     * @param  mixed    $option option data. Data that user did not requested directly but needs them
     * @return ResponseMessage
     */
    public function setOption($option){
        $this->option = $option;
        return $this;
    }

    public function addOption($key, $option){
        $this->option[$key] = $option;
        return $this;
    }

    /**
     * Retrieve option part of message
     * @method getOption
     * @return mixed
     */
    public function getOption(){
        return $this->option;
    }

    /**
     * Set header data. Header data should not be set in Controller, but can be set by other system services. Data will be stored with index in array, so you  can use dot notation to navigate to specific index in header.
     * @method setHeader
     * @param  string    $index  index in header array. You can use dot notation.
     * @param  mixed    $header data
     * @return ResponseMessage
     */
    public function setHeader($index, $header = null){
        if ($header !== null){
            if ($this->header === null){
                $this->header = [];
            }
            array_set($this->header, $index, $header);
        }
        else{
            $this->header = $index;
        }
        return $this;
    }

    /**
     * Retrieve header part of message data.
     * @method getHeader
     * @return mixed
     */
    public function getHeader(){
        return $this->header;
    }

    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    public function getCode() {
        return $this->code;
    }

    /**
     * Form all parts of message into array. Array has indexes 'data', 'option', 'header'.
     * @method toArray
     * @return array
     */
    public function toArray(){
        return [
            'header' => $this->getHeader(),
            'data' => $this->getData(),
            'option' => $this->getOption()
        ];
    }
}
