<?php

namespace Lmn\Core\Lib\Helper;

class AssignService {

    public function __construct() {

    }

    /**
     * Return first value from $values, that doesn't match aby value in $condition list. If they all match $condition then return $def value.
     * @method getDefault
     * @param  array     $values    array of posible returned values
     * @param  mixed     $def       default value that will be returned
     * @param  array     $condition values that cannot be return from $values
     * @return mixed
     */
    public function getDefault($values, $def = null, $condition = [null]) {
        foreach ($values as $v) {
            if (!in_array($v, $condition)) {
                return $v;
            }
        }

        return $def;
    }

    /**
     * Return first value from $values, that doesn't match null 0 or false. If they all match condition then return $def value.
     * @method getDefault
     * @param  array     $values    array of posible returned values
     * @param  mixed     $def       default value that will be returned
     * @return mixed
     */
    public function getDefaultValue($values, $def = null) {
        return $this->getDefaultValue($values, $def, [null, 0, false]);
    }
}
