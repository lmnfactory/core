<?php

namespace Lmn\Core;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;

use Lmn\Core\Command\LmnMigrationCopyCommand;
use Lmn\Core\Command\LmnTestCopyCommand;

use Lmn\Core\Lib\Response\ResponseService;

use Lmn\Core\Lib\Helper\AssignService;

use Lmn\Core\Lib\Request\ConditionRequest;

use Lmn\Core\Lib\Database\GeneratorService;

use Lmn\Core\Lib\Database\Seeder\SeederService;

use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Core\Lib\Structure\StructureService;

use Lmn\Core\Lib\Map\MapService;
use Lmn\Core\Lib\Map\OneToManySingleton;
use Lmn\Core\Lib\Map\OneToOneSingleton;
use Lmn\Core\Lib\Map\OneToOne;

use Lmn\Core\Lib\ArrayStructure\UnorderedArraySingleton;

use Lmn\Core\Lib\Instance\InstanceService;
use Lmn\Core\Lib\Instance\HandleClass;
use Lmn\Core\Lib\Instance\HandleFunction;
use Lmn\Core\Lib\Instance\HandleClosure;
use Lmn\Core\Lib\Instance\HandleObject;

use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Core\Lib\JWT\Validation\JWTTokenValidation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Lib\Model\ValidationRule\ValidationRuleService;

use Lmn\Core\Exception\SystemException;

use Lmn\Core\Lib\Exception\ExceptionService;
use Lmn\Core\Lib\Exception\SystemExceptionHandler;
use Lmn\Core\Lib\Exception\ValidationExceptionHandler;
use Lmn\Core\Lib\Exception\DefaultExceptionHandler;

use Lmn\Core\Lib\Cache\CacheService;

use Lmn\Core\Lib\Search\SearchService;
use Lmn\Core\Lib\Search\SearchFactory;

use Lmn\Core\Lib\Repository\AppoptionsRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Lmn\Core\Repository\Criteria\IdCriteria;
use Lmn\Core\Repository\Criteria\IdsCriteria;
use Lmn\Core\Repository\Criteria\TopCriteria;
use Lmn\Core\Repository\Criteria\LimitCriteria;
use Lmn\Core\Repository\Criteria\PaginationCriteria;
use Lmn\Core\Repository\Criteria\SinceCriteria;
use Lmn\Core\Repository\Criteria\UntilCriteria;
use Lmn\Core\Repository\Criteria\EqCriteria;
use Lmn\Core\Repository\Criteria\InCriteria;
use Lmn\Core\Repository\Criteria\NotInCriteria;
use Lmn\Core\Repository\Criteria\OptionNamespaceCriteria;
use Lmn\Core\Repository\Criteria\RandCriteria;

use Lmn\Core\Lib\Dataase\Select\ConditionCollectionBuilder;
use Lmn\Core\Lib\Dataase\Select\ConditionCollectionService;

use Lmn\Core\Lib\Variable\VariableService;
use Lmn\Core\Lib\Variable\Type\DateConverter;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        $app->singleton(ResponseService::class, ResponseService::class);

        //Helpers
        $app->singleton(AssignService::class, AssignService::class);

        //Generator
        $app->singleton(GeneratorService::class, GeneratorService::class);

        //SaveService
        $provider->getApp()->singleton(SaveService::class, SaveService::class);

        //Seeder
        $provider->getApp()->singleton(SeederService::class, SeederService::class);

        //Instance
        $provider->getApp()->singleton(InstanceService::class, InstanceService::class);

        //Map
        $provider->getApp()->singleton(MapService::class, MapService::class);

        $provider->getApp()->singleton('mapService', StructureService::class);

        //Array
        $provider->getApp()->singleton('arrayService', StructureService::class);

        // JWTService
        $provider->getApp()->singleton(JWTService::class, JWTService::class);

        //Model
        $provider->getApp()->singleton(ValidationService::class, ValidationService::class);

        //ValidationRule
        $provider->getApp()->singleton(ValidationRuleService::class, ValidationRuleService::class);

        //Exception
        $provider->getApp()->singleton(ExceptionService::class, ExceptionService::class);

        //Cache
        $provider->getApp()->singleton(CacheService::class, CacheService::class);

        //Search
        $app->singleton(SearchService::class, SearchService::class);
        $app->singleton(SearchFactory::class, SearchFactory::class);

        // Repository Criteria
        $provider->getApp()->singleton(CriteriaService::class, function($app) {
            $mapServie = $app->make('mapService');
            return new EloquentCriteriaService($app);
        });

        $app->singleton(AppoptionsRepository::class, AppoptionsRepository::class);

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        $instanceService = $provider->getApp()->make(InstanceService::class);
        $instanceService->add(new HandleClass());
        $instanceService->add(new HandleFunction());
        $instanceService->add(new HandleClosure());
        $instanceService->default(new HandleObject());

        $mapService = $provider->getApp()->make('mapService');
        $mapService->add('oneToManySingleton', function() {
            return new OneToManySingleton();
        });
        $mapService->add('oneToOneSingleton', function() {
            return new OneToOneSingleton();
        });
        $mapService->add('oneToOne', function() {
            return new OneToOne();
        });

        $arrayService = $provider->getApp()->make('arrayService');
        $arrayService->add('unorderedSingleton', function() {
            return new UnorderedArraySingleton();
        });

        /** @var ResponseService $responseService */
        $responseService = $app->make(ResponseService::class);
        $validationMessage = $responseService->createMessage("Data are invalid", 422);
        $responseService->prepare('validation.data', $validationMessage);
        $systemValidationMessage = $responseService->createMessage("System validation exception", 400);
        $responseService->prepare('validation.system', $systemValidationMessage);
        $emptyValidationMessage = $responseService->createMessage("", 422);
        $responseService->prepare('validation.empty', $emptyValidationMessage);

        $jwtService = \App::make(JWTService::class);
        $jwtService->setSecret(config('app.secret', false));

        $exceptionService = $provider->getApp()->make(ExceptionService::class);
        $exceptionService->add(SystemException::class, new SystemExceptionHandler())
            ->add(ValidationException::class, new ValidationExceptionHandler());
            //->default(new DefaultExceptionHandler());
        $exceptionService->addDontReport([
            \Illuminate\Auth\AuthenticationException::class,
            \Illuminate\Auth\Access\AuthorizationException::class,
            \Symfony\Component\HttpKernel\Exception\HttpException::class,
            \Illuminate\Database\Eloquent\ModelNotFoundException::class,
            \Illuminate\Session\TokenMismatchException::class,
            \Illuminate\Validation\ValidationException::class,
            ValidationException::class
        ]);

        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $validationService->add('token.jwt', JWTTokenValidation::class);

        \Validator::extend('gt', '\\Lmn\\Core\\Lib\\Model\\ValidationRule\\GtValidationRule@rule');
        \Validator::extend('ge', '\\Lmn\\Core\\Lib\\Model\\ValidationRule\\GeValidationRule@rule');
        \Validator::extend('lt', '\\Lmn\\Core\\Lib\\Model\\ValidationRule\\LtValidationRule@rule');
        \Validator::extend('le', '\\Lmn\\Core\\Lib\\Model\\ValidationRule\\LeValidationRule@rule');
        \Validator::extend('unique_for', '\\Lmn\\Core\\Lib\\Model\\ValidationRule\\UniqueForValidationRule@rule');

        /** @var CriteriaService $criteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('core.id', IdCriteria::class);
        $criteriaService->add('core.ids', IdsCriteria::class);
        $criteriaService->add('core.pagination', PaginationCriteria::class);
        $criteriaService->add('core.top', TopCriteria::class);
        $criteriaService->add('core.limit', LimitCriteria::class);
        $criteriaService->add('core.since', SinceCriteria::class);
        $criteriaService->add('core.until', UntilCriteria::class);
        $criteriaService->add('core.in', InCriteria::class);
        $criteriaService->add('core.notin', NotInCriteria::class);
        $criteriaService->add('core.eq', EqCriteria::class);
        $criteriaService->add('core.option.namespace', OptionNamespaceCriteria::class);
        $criteriaService->add('core.rand', RandCriteria::class);
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Core\\Controller'], function() {
            Route::any('app/cache','CacheController@cache');

            Route::any('api/search','SearchController@search');
        });
    }

    /**
     * Register all commands with Laravel framework (artisan)
     * @method registerCommands
     */
    private function registerCommands($provider){
        $app = $provider->getApp();
        $app['lmn.migration.copy'] = $app->share(function () {
            return new LmnMigrationCopyCommand();
        });
        $app['lmn.test.copy'] = $app->share(function () {
            return new LmnTestCopyCommand();
        });

        $provider->commands('lmn.migration.copy');
        $provider->commands('lmn.test.copy');
    }
}
