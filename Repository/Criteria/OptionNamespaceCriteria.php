<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class OptionNamespaceCriteria implements Criteria {

    private $namespace;

    public function __construct() {

    }

    public function set($args) {
        $this->namespace = $args['namespace'];
    }

    public function apply(Builder $query) {
        $query->where('appoptions.name', 'like', $this->namespace . "%");
    }
}
