<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UntilCriteria implements Criteria {

    private $until;
    private $table;

    public function __construct() {
        $this->table = "";
    }

    public function set($args) {
        $this->until = $args['until'];
        if (isset($args['table'])) {
            $this->table = $args['table'].".";
        }
    }

    public function apply(Builder $query) {
        $query->where($this->table.'created_at', '<', $this->until);
    }
}
