<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class IdCriteria implements Criteria {

    private $id;
    private $table;

    public function __construct() {
        $this->table = null;
    }

    private function getColumn()
    {
        if ($this->table != null) {
            return $this->table . ".id";
        }
        return "id";
    }

    public function set($args) {
        $this->id = $args['id'];
        if (isset($args['table'])) {
            $this->table = $args['table'];
        }
    }

    public function apply(Builder $query) {
        $query->where($this->getColumn(), '=', $this->id);
    }
}
