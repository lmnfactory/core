<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class PaginationCriteria implements Criteria {

    private $page;
    private $limit;

    public function __construct() {

    }

    public function set($args) {
        $this->ids = $args['limit'];
        $this->page = array_get($args, 'page', 1);
    }

    public function apply(Builder $query) {

    }
}
