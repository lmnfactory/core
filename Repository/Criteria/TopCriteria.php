<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class TopCriteria implements Criteria {

    private $limit;

    public function __construct() {

    }

    public function set($args) {
        $this->limit = $args['limit'];
    }

    public function apply(Builder $query) {
        $query->limit($this->limit);
    }
}
