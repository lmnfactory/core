<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SinceCriteria implements Criteria {

    private $since;
    private $table;

    public function __construct() {
        $this->table = "";
    }

    public function set($args) {
        $this->since = $args['since'];
        if (isset($args['table'])) {
            $this->table = $args['table'].".";
        }
    }

    public function apply(Builder $query) {
        $query->where($this->table.'created_at', '>', $this->since);
    }
}
