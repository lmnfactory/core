<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class IdsCriteria implements Criteria {

    private $ids;

    public function __construct() {

    }

    public function set($args) {
        $this->ids = $args['ids'];
    }

    public function apply(Builder $query) {
        $query->whereIn('id', $this->ids);
    }
}
