<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class NotInCriteria implements Criteria {

    private $value;
    private $name;

    public function __construct() {

    }

    public function set($args) {
        $this->value = $args['value'];
        $this->name = $args['name'];
    }

    public function apply(Builder $query) {
        $query->whereNotIn($this->name, $this->value);
    }
}
