<?php

namespace Lmn\Core\Repository\Criteria;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class RandCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) { }

    public function apply(Builder $query) {
        $query->orderBy(\DB::raw('RAND()'));
    }
}
